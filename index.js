const hydraExpress = require('hydra-express')
const hydra = hydraExpress.getHydra()
const env = require('node-env-file')
const pkg = require('./package.json')
const util = require('util')
const cvp = require('cvp-vxml-client')

// load environment file
env(__dirname + '/.env')

// set up hydra and redis config
const hydraConfig = {
  hydra: {
    serviceName: pkg.name,
    serviceIP: process.env.hydra_service_ip || '',
    servicePort: process.env.hydra_service_port || 0,
    serviceType: process.env.hydra_service_type || '',
    serviceDescription: pkg.description,
    redis: {
      url: process.env.redis_url,
      port: process.env.redis_port,
      db: process.env.redis_db
    }
  }
}

// define routes
function onRegisterRoutes() {
  var express = hydraExpress.getExpress()
  var api = express.Router()

  api.get('/test/context-service', async function(req, res) {
    console.log('hydraExpress request to test context service. query:', req.query)
    try {
      let cvpConfig = {
        host: 'cvp1.dcloud.cisco.com',
        port: '7000',
        app: 'Get_Customer',
        dnis: '7700',
        ani: req.query.ani,
        params: {
          'q': req.query.ani,
          'field': 'query_string'
        }
      }
      let result = await cvp.testContextService(cvpConfig)
      console.log(result)
      res.status(200).send({result})
    } catch (err) {
      // ... error checks
      console.log(err)
      res.status(500).send({err})
    }
  })

  hydraExpress.registerRoutes({
    '': api
  })
}

// start up
hydraExpress.init(hydraConfig, onRegisterRoutes)
.then(serviceInfo => {
  console.log('serviceInfo', serviceInfo)
  // return 0
})
.catch(err => console.log('err', err))
