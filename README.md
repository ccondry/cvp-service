# cvp-service
A HydraExpress microservice that can interact with the Cisco CVP OAMP and VXML REST services.

## Usage
Start the service with `node .`

To test, run `hydra-cli rest cvp-service:[get]/test/context-service?ani=5551112222`
